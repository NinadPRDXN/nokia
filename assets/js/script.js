/* Author: 
    Ninad
*/
var 
submit = document.querySelector('input[type="submit"]'),
list = document.querySelector('.list'),
validity = /^[0-9]+$/;

submit.onclick = function(e) {

    var
    user_input = document.querySelector('input[type="text"]').value;
    e.preventDefault();

    if(user_input.length != 0 && validity.test(user_input)) {
        var
        //To Convert Array Elments in Integer
        ar = user_input.split(''),
        result = ar.map(function (x) {
            return parseInt(x, 10); 
        });

        //Letter as per Nokia Phone Key pad
        keypad_mapings = [
            [],
            [],
            ['a', 'b', 'c'],
            ['d', 'e', 'f'],
            ['g', 'h', 'i'],
            ['j', 'k', 'l'],
            ['m', 'n', 'o'],
            ['p', 'q', 'r', 's'],
            ['t', 'u', 'v'],
            ['w', 'x', 'y', 'z']
        ];

        get_all_words(result);
    }
    else {
        list.innerHTML = "Invalid Input";
    }
}

function get_all_words(pressed_buttons) {
    var letters = [];
    //Pushing the respective letters of number which is pressed by User
    for (i = 0; i < pressed_buttons.length; i++) {
        letters.push(keypad_mapings[pressed_buttons[i]]);
    }

    var progress = 0;
    current_word = '';
    limit = pressed_buttons.length;
    found_words = [];
    
    var return_value = recursive_word_generator(letters, progress, current_word, limit, found_words);

    //To Display the final Outcome.
    list.innerHTML = "";
    return_value.forEach(display);
    
    function display(item) {
        list.innerHTML += item + '<br>';
    }
}


function recursive_word_generator(letters, progress, current_word, limit, found_words) {

    // completed word, add to collection and return
    if (progress == limit) {
        found_words.push(current_word);
    } else {
        // make recursive call for each letter in current press
        for (var i = 0; i < letters[progress].length; i++) {
            var next_word = current_word + letters[progress][i];

            recursive_word_generator(letters, progress + 1, next_word, limit, found_words);
        }
    }
    return found_words
}


















